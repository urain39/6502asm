; by urain39@cyfan.cf

jmp main

init:
 lda #00
 sta $00
 lda #02
 sta $01
 lda #00
 sta $02 ; write color code
 rts 

drawPoint:
 lda $02 ; read color code
 sta ($00), y
 rts

nextBlock:
 inc $01
 lda $01
 cmp #$06
 beq init
 bpl init 
 rts

nextLine:
 tya
 adc #32
 tay
 rts

main:
 jsr init
 loop:
  lda $fe ; color = rnd
  sta $02
  ldy $fe ; position = rnd
  jsr drawPoint
  jsr nextLine
  jsr nextBlock
 jmp loop
 rts

